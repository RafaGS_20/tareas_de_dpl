// Primer programa con JavaScript.

// 
var http = require ('http');
var puerto = 3000;

// Creo un servidor que tendrá una conducta definida. 
http.createServer(function(req,res) {
res.writeHead(200, {'Content-Type': 'text/html'});
res.end('Rafael Gonzalez Sanchez');
}).listen (puerto); // Puerto que escucha. 

//Como el imprimir está fuera del evento sale al activar el programa. 
console.log ('Servidor corriendo en el puerto ' + puerto);