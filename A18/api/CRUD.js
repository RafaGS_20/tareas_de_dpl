let mongoose = require ('mongoose'); 

let Schema = mongoose.Schema;

let accountsSchema = new Schema ({

    name: String, 

    balance: Number

});

module.exports = mongoose.model ( "Cuentas", accountsSchema);
