// Importación de los módulos que necesitaremos. 
const express = require('express') 
const logger = require('morgan')
const mongodb= require('mongodb')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const Cuentas = require('./api/CRUD')

// Definimos la conexión con el mongo. 
const url = 'mongodb://localhost:27017/accounts'
let app = express()
app.use(logger('dev'))
app.use(bodyParser.json())

mongoose.connect( url, { useUnifiedTopology: true, useNewUrlParser: true  });
mongoose.Promise = global.Promise; 

var db = mongoose.connection; 

// Da errores en caso de no poder conectar. 

db.on("error", console.error.bind('Error de conexión con MongoDB'));

// Primer método, GET. 

app.get('/accounts', (req, res) => {
    Cuentas.find({}, function(error, accounts){
        if (error) res.status(500).send(error.message);
        res.status(200).json({
            Cuentas: accounts
        }) 
    })
})

// Create. 

app.post('/accounts', (req,res) => {
    Cuentas.create(req.body, function(error) {
        if(error) res.status(500).send(error.message);
        res.status(200).send("Datos subidos.");
    })
})

// Modificación. 
app.put('/accounts/:id', (req, res) =>{
    Cuentas.findByIdAndUpdate(req.params.id, req.body, function(error) {
        if(error) res.status(500).send(error.message); 
        res.status(200).send("Datos actualizados.");
    })
})

// Delete. 
app.delete('/accounts/:id', (req,res) => {
    Cuentas.findByIdAndDelete(req.params.id, function(error) {
        if(error) res.status(500).send(error.message);
        res.status(200).send("Borrado.");
    })
})


// Escucha en el puerto 3000. 

app.listen(3000);

