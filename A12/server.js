// Importación de los módulos que necesitaremos. 
const express = require('express') 
const logger = require('morgan')
const errorhandler = require('errorhandler')
const mongodb= require('mongodb')
const bodyParser = require('body-parser')

// Definimos la conexión con el mongo. 
const url = 'mongodb://localhost:27017/accounts'
let app = express()
app.use(logger('dev'))
app.use(bodyParser.json())

// Para conseguir las cuentas: GET/accounts.
mongodb.MongoClient.connect(url, { useUnifiedTopology: true }, (error, client) => {
    if (error) return process.exit(1)
  
    var db = client.db('accounts')
    app.get('/accounts', (req, res) => {
      db.collection('accounts')
        .find({}, {sort: {_id: -1}})
        .toArray((error, accounts) => {
          if (error) return next(error)
          res.send(accounts)
      })
    })

// Para enviar y poner cuentas: POST/accounts and PUT/accounts.
app.post('/accounts', (req, res) => {
    let newAccount = req.body
    db.collection('accounts').insert(newAccount, (error, results) => {
      if (error) return next(error)
      res.send(results)
    })
  })

app.put('/accounts/:id', (req, res) => {
    db.collection('accounts')
      .updateOne({_id: mongodb.ObjectID(req.params.id)}, 
        {$set: req.body}, 
        (error, results) => {
          if (error) return next(error)
          res.send(results)
        }
      )
   })

// Para borrar los datos: DELETE/accounts.
app.delete('/accounts/:id', (req, res) => {
    db.collection('accounts')
      .deleteOne({_id: mongodb.ObjectID(req.params.id)}, 
        (error, results) => {
          if (error) return next(error)
          res.send(results)
        }
      )
   })
})

// Escucha en el puerto 3000. 

app.listen(3000);