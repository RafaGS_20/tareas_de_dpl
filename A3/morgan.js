app.get('/about', function (req, res){
    res.send ('Me llamo Rafael González Sánchez. Tengo 27 años. Mi comida favorita es la lasaña.');
});

app.listen(port, () => {
    console.log('Aplicación ejecutándose en el puerto ${port}!');
});