exports.suma = (numero1, numero2 ) => { return numero1 + numero2; }; // Definimos la función con sus nombres, variables a usar y operación a realizar. 
exports.resta = (numero1, numero2) => { return numero1 - numero2; };
exports.multiplicacion = (numero1, numero2) => { return numero1 * numero2; };
exports.division = (numero1, numero2) => { return numero1 / numero2; }; 
exports.modulo1 = (numero1) => { return numero1 % 2; };
exports.modulo2 = (numero2) => { return numero2 % 2; };