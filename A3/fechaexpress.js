var express = require ('express');
var app = express();

app.get('/', function (req, res) {
    var hora = new Date(); // Función que pilla la hora actual. 
    var Hora = hora.toString();
    res.send (Hora);
});

app.get('/about', function (req, res) {
    res.send ('Me llamo Rafael González Sánchez. Tengo 27 años y mi comida favorita es la lasaña.');
});

app.listen (3000, function() {
    console.log ('Aplicación con express en el puerto 3000.');
});

